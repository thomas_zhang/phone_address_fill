"""
phone and address extractor from raw html

Written by Thomas Zhang: thomas@owler-inc.com on Nov 2, 2017.
-- Nov 7, 2017: changed cleaning logic for better rendering
"""
import re
import requests
from bs4 import BeautifulSoup
import pyap


def clean_html(html):
    soup = BeautifulSoup(html)

    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()  # rip it out

    # get text
    text = soup.get_text()

    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())

    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))

    # drop blank lines
    text = ' '.join(chunk for chunk in chunks if chunk)
    return text


def extract_phone_numbers(string):
    r = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
    phone_numbers = r.findall(string)
    res = [re.sub(r'\D', '', number) for number in phone_numbers]
    return set(res)


def main():
    """ example."""
    link = 'https://quid.com/our-story'
    html = requests.get(link).content
    string = clean_html(html)
    phones = extract_phone_numbers(string)
    address = pyap.parse(string, country='US')
    print('phones ', phones)

    print('address ')
    for e in address:
        print(e)


if __name__ == "__main__":
    main()
